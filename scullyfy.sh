#!/bin/bash

languages=(en de)

rm -rf dist/static

for language in "${languages[@]}"
do
    index_file="dist/localization/$language/index.html"
    echo "Replace base href for $language in $index_file"
    sed -i -e 's/"\/'$language'\/"/"\/"/g' $index_file
    echo "Build scully for $language"
    LOCALE=$language npm run scully -- --scanRoutes
    LOCALE=$language npm run scully
done

cp htaccess.file dist/static/.htaccess
