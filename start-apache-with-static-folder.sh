#!/bin/bash


docker build -t localization:latest -f apache.Dockerfile .
docker run -it --rm --name localization -v "$PWD/dist/static":/usr/local/apache2/htdocs/ -p 8080:80 localization:latest
